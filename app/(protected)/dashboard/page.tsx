import { auth } from '@clerk/nextjs';
import React from 'react';

export default function Dashboard() {
  const { user, userId } = auth();

  console.log(user, userId);

  return (
    <div>
      <p>hello</p>
      <p>{JSON.stringify(user, null, 3)}</p>
    </div>
  );
}
