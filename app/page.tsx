'use client';

import { Button } from '@mantine/core';
import Link from 'next/link';
import { HeroSection } from 'components/HeroSection/HeroSection';
import { Welcome } from '../components/Welcome/Welcome';

export default function HomePage() {
  return (
    <>
      <HeroSection />
      {/* <Welcome />
      <Button component={Link} href="/dashboard">
        next page
      </Button> */}
    </>
  );
}
