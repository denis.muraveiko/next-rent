'use client';

import { SignUp } from '@clerk/nextjs';
import { Group } from '@mantine/core';
import classes from './signup.module.css';

export default function HomePage() {
  return (
    <>
      <Group justify="center" mt="xl">
        <SignUp
          appearance={{
            elements: {
              card: classes.card,
              formFieldInput: classes.formButtonPrimary,
            },
          }}
          signInUrl="/auth/signin"
        />
      </Group>
    </>
  );
}
