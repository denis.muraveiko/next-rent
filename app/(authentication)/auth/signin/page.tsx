'use client';

import { SignIn } from '@clerk/nextjs';
import { Group } from '@mantine/core';
import classes from './signin.module.css';

export default function HomePage() {
  return (
    <>
      <Group justify="center" mt="xl">
        <SignIn
          appearance={{
            elements: {
              card: classes.card,
              formFieldInput: classes.formButtonPrimary,
            },
          }}
          signUpUrl="/auth/signup"
        />
      </Group>
    </>
  );
}
