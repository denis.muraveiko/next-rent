import '@mantine/core/styles.css';
import '@mantine/notifications/styles.css';

import { ColorSchemeScript } from '@mantine/core';
import { ClerkProvider } from '@clerk/nextjs';
import { dark } from '@clerk/themes';

import Providers from 'components/Providers';
import { Navbar } from 'components/Navbar/Navbar';

export const metadata = {
  title: 'Mantine Next.js template',
  description: 'I am using Mantine with Next.js!',
};

export default function RootLayout({ children }: { children: React.ReactNode }) {
  return (
    <ClerkProvider
      appearance={{
        baseTheme: dark,
        variables: {
          colorPrimary: '#099268',
          colorBackground: 'rgb(40, 44, 52)',
        },
      }}
    >
      <html lang="en">
        <head>
          <ColorSchemeScript defaultColorScheme="dark" />
          <link rel="shortcut icon" href="/favicon.svg" />
          <meta
            name="viewport"
            content="minimum-scale=1, initial-scale=1, width=device-width, user-scalable=no"
          />
        </head>
        <body>
          <Providers>
            <Navbar />
            {children}
          </Providers>
        </body>
      </html>
    </ClerkProvider>
  );
}
