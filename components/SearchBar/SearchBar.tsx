import React, { useCallback, useState } from 'react';
import { Kbd, TextInput, Loader, Autocomplete } from '@mantine/core';
import { SearchOutlined } from '@mui/icons-material';
import styled from '@emotion/styled';
import classes from './SearchBar.module.css';

const StyledTextInput = styled(Autocomplete)`
  &:focus-within {
    input {
      // border-color: var(--mantine-color-dimmed);
    }
  }
`;

export function SearchBar() {
  const [value, setValue] = useState('');
  const [loading, setLoading] = useState(false);
  const [data, setData] = useState<string[]>([]);

  const handleChange = useCallback((search: string) => {
    setValue(search);
  }, []);

  return (
    <StyledTextInput
      style={{ outlineColor: 'red' }}
      placeholder="Search a car near you"
      size="lg"
      radius="md"
      leftSection={<SearchOutlined />}
      value={value}
      data={[
        { group: 'Frontend', items: ['React', 'Angular'] },
        { group: 'Backend', items: ['Express', 'Django'] },
      ]}
      onChange={handleChange}
      rightSection={loading ? <Loader size="1rem" /> : null}
      withScrollArea={false}
      styles={{ dropdown: { maxHeight: 200, overflowY: 'auto' } }}
      comboboxProps={{ transitionProps: { transition: 'pop', duration: 200 } }}
    />
  );
}
