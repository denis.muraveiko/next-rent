import React from 'react';
import classes from './Skeleton.module.css';

export const Skeleton = ({ size }: { size: string }) => {
  const style = {
    width: size,
    height: size,
    borderRadius: '50%',
  };

  return <div className={classes.skeleton} style={style} />;
};
