'use client';

import { MantineProvider } from '@mantine/core';
import { Notifications } from '@mantine/notifications';

import { theme } from 'theme';

const Providers = ({ children }: { children: React.ReactNode }) => (
  <MantineProvider theme={theme} defaultColorScheme="dark">
    <Notifications />
    {children}
  </MantineProvider>
);

export default Providers;
