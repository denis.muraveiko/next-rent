import { Container, Text, Button, Group, Box } from '@mantine/core';
import { SearchBar } from 'components/SearchBar/SearchBar';
import classes from './HeroTitle.module.css';
import { Dots } from './Dots';

export function HeroTitle() {
  return (
    <div className={classes.wrapper}>
      <Dots className={classes.dots} style={{ left: 0, top: 0 }} />
      <Dots className={classes.dots} style={{ left: 60, top: 0 }} />
      <Dots className={classes.dots} style={{ left: 0, top: 140 }} />
      <Dots className={`${classes.dots} ${classes.dotsHide}`} style={{ right: 0, top: 60 }} />

      <Container size={700} className={classes.inner}>
        <h1 className={classes.title}>
          Ваша{' '}
          <Text
            component="span"
            variant="gradient"
            gradient={{ from: 'teal', to: 'green' }}
            inherit
          >
            идеальная
          </Text>{' '}
          поездка начинается здесь
        </h1>

        <Text className={classes.description} c="dimmed">
          Откройте для себя удобство аренды автомобилей с нами - исследуйте мир с комфортом и стилем
        </Text>

        <Group className={classes.controls}>
          <Box style={{ flexGrow: 1 }}>
            <SearchBar />
          </Box>

          <Button
            size="lg"
            radius="md"
            className={classes.control}
            variant="gradient"
            gradient={{ from: 'blue', to: 'cyan' }}
            style={{ minWidth: '64px' }}
          >
            Search
          </Button>
        </Group>
      </Container>
    </div>
  );
}
