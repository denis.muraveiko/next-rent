import { UserButton, useUser } from '@clerk/nextjs';
import { Button, Loader } from '@mantine/core';
import Link from 'next/link';
import { usePathname } from 'next/navigation';

export function NavbarAvatar() {
  const { user, isLoaded } = useUser();
  const pathname = usePathname();

  if (pathname.includes('/auth/')) {
    return null;
  }

  if (!isLoaded) {
    return <Loader size="32px" color="gray" />;
  }

  return user ? (
    <UserButton afterSignOutUrl="/" />
  ) : (
    <Button component={Link} href="/auth/signin" loading={!isLoaded}>
      Log in
    </Button>
  );
}
