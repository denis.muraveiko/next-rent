'use client';

import { Quicksand } from 'next/font/google';
import { createTheme } from '@mantine/core';

const quicksand = Quicksand({ subsets: ['latin'] });

export const theme = createTheme({
  primaryColor: 'teal',
  fontFamily: quicksand.style.fontFamily,
});
